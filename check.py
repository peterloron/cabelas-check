#!/usr/bin/env python3

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.firefox.options import Options
import time
import sys
import smtplib

timeout = 30

products = [
  {
    "url": "https://www.cabelas.com/shop/en/herters-target-handgun-ammo",
    "items": [
      { "code": "3074457345618973127", "desc": "Herters 9mm 115gr 50rd" }
    ]
  },
  {
    "url": "https://www.cabelas.com/shop/en/winchester-usa-handgun-ammo",
    "items": [
      { "code": "73894", "desc": "Win White Box 9mm 115gr 50rd", "sku": "691518" }
    ]
  },
  {
    "url": "https://www.cabelas.com/shop/en/blazer-brass-handgun-ammo",
    "items": [
      { "code": "3074457345616736948", "desc": "Blazer 9mm 124gr 50rd", "sku": "2352268" }
    ]
  },
  {
    "url": "https://www.cabelas.com/shop/en/federal-american-eagle-centerfire-handgun-ammo",
    "items": [
      { "code": "131828","desc": "Fed American Eagle 9mm 115gr 50rd", "sku": "" },
      { "code": "3074457345616932618","desc": "Fed American Eagle 9mm 115gr 100rd", "sku": "2025695" },
      { "code": "3074457345616700134", "desc": "Fed American Eagle 9mm 147gr 50rd", "sku": "2352453" }
    ]
  },
  {
    "url": "https://www.cabelas.com/shop/en/winchester-usa-handgun-ammo-bulk-pack",
    "items": [
      { "code": "3074457345617437113", "desc": "Win White Box 9mm 115gr 500rd", "sku": "2388969" }
    ]
  },
  {
    "url": "https://www.cabelas.com/shop/en/cci-mini-mag-rimfire-ammo",
    "items": [
      { "code": "131287", "desc": "CCI MiniMag 40gr 100rd", "sku": "1890719" }
    ]
  },
  {
    "url": "https://www.cabelas.com/shop/en/federal-champion-brass-centerfire-handgun-ammo",
    "items": [
      { "code": "3074457345620130797", "desc": "Federal Champ 9mm 115gr 50rd", "sku": "3193780" }
    ]
  },
  {
    "url": "https://www.cabelas.com/shop/en/remington-umc-centerfire-handgun-ammo",
    "items": [
      { "code": "82825", "desc": "Rem UMC 9mm 115gr 100rd", "sku": "1049133" }
    ]
  },
  {
    "url": "https://www.cabelas.com/shop/en/450-406-100122828",
    "items": [
      { "code": "3074457345618731663", "desc": "Sig 9mm 115gr 200rd", "sku": "4415329" }
    ]
  },
  {
    "url": "https://www.cabelas.com/shop/en/winchester-super-suppressed-handgun-ammo",
    "items": [
      { "code": "3074457345618309789", "desc": "Win Super Suppressed 9mm 147gr 50rd", "sku": "2494777" }
    ]
  }
]

targets = {
  "Ship to store": "in_store_status_",
  "Ship to home ": "WC_Online_Inventory_Section_",
  "Store pickup ": "pickup_status_"
}

cookies = [
{
  "name": "BPS_Physical_Store",
  "value": "78",
  "domain": "www.cabelas.com",
  "path": "/",

},
{
  "name": "BPS_Physical_Store_Data",
  "value": "%7B%22meta%22%3A%7B%22uuid%22%3A%221f2c2473-ada7-4b85-a296-8267e4662492%22%2C%22errors%22%3A%5B%5D%7D%2C%22response%22%3A%7B%22zip%22%3A%2298408%22%2C%22hours%22%3A%221%3A10%3A00%3A19%3A00%2C2%3A9%3A00%3A20%3A00%2C3%3A9%3A00%3A20%3A00%2C4%3A9%3A00%3A20%3A00%2C5%3A9%3A00%3A20%3A00%2C6%3A9%3A00%3A20%3A00%2C7%3A9%3A00%3A20%3A00%22%2C%22locationName%22%3A%22Bass%20Pro%20Shops%22%2C%22address%22%3A%227905%20S%20Hosmer%20St%22%2C%22city%22%3A%22Tacoma%22%2C%22customFields%22%3A%7B%2214326%22%3A%22https%3A%2F%2Fstores.basspro.com%2Fus%2Fwa%2Ftacoma%2F7905-s-hosmer-st.html%22%2C%22166967%22%3A%22https%3A%2F%2Fmaps.google.com%2F%3Fcid%3D3697348361185979952%22%2C%22180256%22%3A%22https%3A%2F%2Fbasspro.scene7.com%2Fis%2Fimage%2FBassPro%2Flogo_bps-wood%3F%24BPSITE_BPSstoreloc%24%22%7D%2C%22holidayHours%22%3A%5B%7B%22date%22%3A%222020-12-25%22%2C%22hours%22%3A%22%22%2C%22isRegularHours%22%3Afalse%7D%5D%2C%22yextDisplayLng%22%3A-122.4609747%2C%22phone%22%3A%222536715700%22%2C%22closed%22%3A%7B%22isClosed%22%3Afalse%7D%2C%22id%22%3A%2278%22%2C%22state%22%3A%22WA%22%2C%22yextDisplayLat%22%3A47.1853945%7D%7D",
  "domain": "www.cabelas.com",
  "path": "/"
}
]


def notify(message):
  try:
    print("Sending message...")
    smtpObj = smtplib.SMTP('smtp.gmail.com', 587)
    smtpObj.starttls()
    smtpObj.login('peterloron@gmail.com','')
    smtpObj.sendmail('peterloron@gmail.com', 'peterloron@protonmail.com', message,)
    print("Message sent.")
  except smtplib.SMTPException as e:
    print(f"Unable to send email: {e}")

def checkIt(browser, product):
  global timeout

  for item in product['items']:
    #print(f"\nChecking: {item['desc']}\n--------------------------------------------")
    for k,v in targets.items():
      target = f"{v}{item['code']}"
      x = 0
      nf = False

      # is the thing there at all?
      if len(browser.find_elements('id', target)) == 0:
        #print(f"Item {item['desc']} is not shown.")
        break

      # hit the check store link if needed
      lnk = browser.find_element_by_id(f"WC_Check_Stores_Link_{item['code']}")
      if lnk.is_displayed() == True:
        #print("Clicking link...")
        time.sleep(5)
        lnk.click()
        time.sleep(2)
      while x < timeout and nf == False:
        try:
          if browser.find_element_by_id(target).text != "":
            break
          else:
            time.sleep(1)
            print(".")
            x=x+1
        except NoSuchElementException as e:
          print(f"Cannot find {target}.\n")
          nf = True
          continue
        except Exception as e:
          print(f"EXCEPTION: {e}")

      status = browser.find_element_by_id(target).text.strip()
      if status != "" and status != "Out of Stock":
        notify(f"{item['desc']} is {status}")
        print(f"{item['desc']} - {k}: {browser.find_element_by_id(target).text}")

def main():
  #notify("Hi there")

  options = Options()
  options.headless = True
  options.log.level = "error"
  browser = webdriver.Firefox(options=options)

  #print("Doing initial page load to set cookies...")
  browser.get("https://www.cabelas.com")

  for cookie in cookies:
    browser.add_cookie(cookie)

  div = browser.find_element_by_class_name("suwClose")
  if div.is_displayed():
    div.click()


  for product in products:
    browser.get(product['url'])
    checkIt(browser, product)
    #print("\n")

  browser.quit()
  print("Done")

if __name__ == "__main__":
  main()